<?php

/**
 * @file
 * The mist user interface.
 */

/**
 * Builder function for the mist settings form.
 */
function mist_admin_form($form, $form_state) {
  $form['mist_next_language'] = array(
    '#type' => 'checkbox',
    '#title' => t('Redirect to the next untranslated language'),
    '#description' => t('When this is enabled, the <strong>Save and translate</strong> button will load an <em>Add translation</em> form for the next untranslated language instead of the general translation page. Useful and faster when adding many new links and translations at once.'),
    '#default_value' => variable_get('mist_next_language', TRUE),
  );

  $form = system_settings_form($form);

  return $form;
}
