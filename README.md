## INTRODUCTION ##

**Menu Item Save and Translate** (MIST) is a small companion module to
[i18n_menu](http://drupal.org/project/i18n) providing a *Save and Translate*
button on a menu's *Add link* page.

The normal *i18n_menu* translation process requires manually finding again which
menu link to translate, 3 clicks and 2 pages loads making the process very slow,
painful and error prone.

MIST fixes that by cycling through all languages during link creation allowing a
very fast translation process when manually adding menu links. When clicking on
*Save and Translate*, MIST will load the "add link" form with the next language
pre-selected, in weighted order.

Caveat: when using more than 2 languages and saving and translating a *language
neutral* menu item, the redirection will only happen for the second language
as the interface is different when using localization.


## REQUIREMENTS ##

This module requires the following modules:

 * [i18n_menu](https://drupal.org/project/i18n)


## INSTALLATION ##

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


## CONFIGURATION ##

MIST's default behavior is to cycle through all untranslated languages in their
weighted order.

There is a setting to change this behavior at admin/config/regional/mist

When disabled, the user will be redirected to the menu item's general
translation page instead.


## MAINTAINERS ##

Current maintainers:

 * [Philippe T. Simard (ptsimard)](https://www.drupal.org/user/1972814)
